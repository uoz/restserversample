package org.utkuozdemir.restserversample.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.utkuozdemir.restserversample.dto.HelloDto;
import org.utkuozdemir.restserversample.dto.Person;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Enumeration;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Utku on 29.6.2014.
 */
@RestController
public class ExampleController {
	@RequestMapping(value = "/getExample", method = RequestMethod.GET)
	public String getExample(HttpServletRequest request) {
		Map<String, String> headers = extractHeaders(request);
		System.out.println(headers);
		return "Hello World!";
	}

	@RequestMapping(value = "/getExampleWithJsonReturnType",
			method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public HelloDto getExampleWithJsonReturnType() {
		return new HelloDto("Hello World", new Date());
	}

	@RequestMapping(value = "/getExampleWithXmlReturnType",
			method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
	public HelloDto getExampleWithXmlReturnType() {
		return new HelloDto("Hello World", new Date());
	}

	@RequestMapping(value = "/getExampleWithPathVariable/{pathVariable}", method = RequestMethod.GET)
	public String getExampleWithPathVariable(@PathVariable("pathVariable") String pathVariable) {
		return "Your path variable: " + pathVariable;
	}

	@RequestMapping(value = "/getExampleWithQueryStringParameter", method = RequestMethod.GET)
	public String getExampleWithQueryStringParameter(@RequestParam(value = "parameter") String parameter) {
		return "Your parameter: " + parameter;
	}

	/**
	 * Send a person as JSON. Example
	 * {
	 *     "firstName": "Utku",
	 *     "lastName": "Ozdemir",
	 *     "age": 24
	 * }
	 *
	 * DONT FORGET TO SET "Content-Type" HEADER FLAG TO "application/json"
	 *
	 * @param person person object as JSON in the raw post request body
	 * @return Person's details as a string
	 */
	@RequestMapping(value = "/postExampleWithJsonRequestBody", method = RequestMethod.POST)
	public String postExampleWithJsonRequestBody(@RequestBody Person person) {
		return person.getFirstName() + " " + person.getLastName() + " is " + person.getAge() + " years old.";
	}

	// SECURED EXAMPLES

	@RequestMapping(value = "/secured/getExample", method = RequestMethod.GET)
	public String securedGetExample() {
		return "Hello World!";
	}

	@RequestMapping(value = "/secured/postExampleWithJsonRequestBody", method = RequestMethod.POST)
	public String securedPostExampleWithJsonRequestBody(@RequestBody Person person) {
		return person.getFirstName() + " " + person.getLastName() + " is " + person.getAge() + " years old.";
	}

	private Map<String, String> extractHeaders(HttpServletRequest request) {
		Map<String, String> headers = new TreeMap<>();
		Enumeration<String> requestHeaderNames = request.getHeaderNames();
		while (requestHeaderNames.hasMoreElements()) {
			String headerKey = requestHeaderNames.nextElement();
			headers.put(headerKey, request.getHeader(headerKey));
		}
		return headers;
	}
}
