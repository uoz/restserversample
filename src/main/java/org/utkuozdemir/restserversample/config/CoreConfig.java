package org.utkuozdemir.restserversample.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Utku on 29.6.2014.
 */
@Configuration
@ComponentScan(basePackageClasses = {ZzzzScan.class})
public class CoreConfig {
}
