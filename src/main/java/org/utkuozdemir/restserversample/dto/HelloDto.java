package org.utkuozdemir.restserversample.dto;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * Created by Utku on 29.6.2014.
 */
@SuppressWarnings("unused")
@XmlRootElement(name = "helloDto")
public class HelloDto {
	private String text;
	private Date currentDate;

	public HelloDto() {
	}

	public HelloDto(String text, Date currentDate) {
		this.text = text;
		this.currentDate = currentDate;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}
}
