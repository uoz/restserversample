package org.utkuozdemir.restserversample;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Map;

/**
 * Created by Utku on 12.8.2014.
 */
public class TestLauncher {
	private static Logger logger = LoggerFactory.getLogger(TestLauncher.class);

	public static void main(String[] args) {
		try {
			String xmlUrl = "http://localhost:8081/getExampleWithXmlReturnType";
			String jsonUrl = "http://localhost:8081/getExampleWithJsonReturnType";
//			Jaxb2RootElementHttpMessageConverter xmlConverter = new Jaxb2RootElementHttpMessageConverter();
//			MappingJackson2HttpMessageConverter jsonConverter = new MappingJackson2HttpMessageConverter();
//			List<HttpMessageConverter<?>> converters = new ArrayList<>();
//			converters.add(xmlConverter);
//			converters.add(jsonConverter);

			ObjectMapper objectMapper = new ObjectMapper();
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Arrays.asList(MediaType.ALL));

			HttpEntity<String> entity = new HttpEntity<>("parameters", headers);

			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> result = restTemplate.exchange(jsonUrl, HttpMethod.GET, entity, String.class);

			if (isValidJSON(result.getBody())) {
				@SuppressWarnings("unchecked")
				Map<String, Object> map = objectMapper.readValue(result.getBody(), Map.class);
				System.out.println(map);
			}

			System.out.println(result);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	private static boolean isValidJSON(final String json) {
		boolean valid;
		try {
			final JsonParser parser = new ObjectMapper().getFactory()
					.createParser(json);
			//noinspection StatementWithEmptyBody
			while (parser.nextToken() != null) {
			}
			valid = true;
		} catch (Exception e) {
			valid = false;
		}
		return valid;
	}
}
